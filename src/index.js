export default class SwapiService {

    _apiBase = 'https://swapi.dev/api';

    async getResource(url) {

        const res = await fetch(`${this._apiBase}${url}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();

    }

    //People

    async getAllPeople() {
        const res = await this.getResource(`/people/`);
        return res.results;
    }

    getPerson(id) {
        return this.getResource(`/people/${id}/`);
    }

    //Starships
    async getAllStarships() {
        const res = await this.getResource(`/starships/`);
        return res.results;
    }

    getStarships(id) {
        return this.getResource(`/starships/${id}/`);
    }

    //Planets
    async getAllPlanets() {
        const res = await this.getResource(`/planets/`);
        return res.results;
    }

    getPlanets(id) {
        return this.getResource(`/planets/${id}/`)
    }

}

const swapi = new SwapiService();

swapi.getPerson(3).then((players) => {
    console.log(players.name);
});

swapi.getStarships(3).then((starships) => {
    console.log(starships.name);
});

swapi.getPlanets(3).then((planets) => {
    console.log(planets.name);
});

