import Header from "../header";
import ItemList from "../item-list";
import PersonDetails from "../person-details";
import RandomPlanet from "../random-planet";



const App = () => {
    return (
        <div>
            <Header />
            <RandomPlanet />
            <div className="row mb2">
                <div className="cool-mb-6">
                    <ItemList />
                </div>
                <div className="cool-mb-6">
                    <PersonDetails />
                </div>

        </div>

    )
}
